import React from 'react';

export function Header(){
    return (
        <header>
            <h1>HN Feed</h1>
            <h3>We &lt;3 Hacker News!</h3>
        </header>
    )
}
