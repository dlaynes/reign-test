import React, { useState } from 'react';
import { httpRemoveStory } from '../services/http/api';

import ReactTimeAgo from 'react-time-ago'

import img from '../assets/delete-photo-svgrepo-com.svg';

export function ListItem(props){
    const [deleting, setDeleting] = useState(false);
    const [deleted, setDeleted] = useState(false);

    const handleDelete = async function(){
        try {
            setDeleting(true);
            const res = await httpRemoveStory(props.item.objectID);
            setDeleting(false);
            if(res.ok){
                setDeleted(true);
            }
        } catch(e){
            setDeleting(false);
            window.alert("Error when deleting a story");
        }
        return false;
    };

    if(deleted || (!props.item.story_title && !props.item.title)) return null;

    console.log("Created at", props.item.created_at);

    return (
        <li className="list-item">
            <a href={props.item.story_url} target="_blank" rel="noreferrer">
                <span>{props.item.story_title ? props.item.story_title : props.item.title}</span>
                {props.item.author && <address className="author">- {props.item.author} -</address>}
            </a>
            {props.item.created_at && <ReactTimeAgo date={props.item.created_at} locale="en-US"/>}
            {!deleting && <input type="image" src={img} onClick={handleDelete} alt="Delete" />}
        </li>
    );
}
