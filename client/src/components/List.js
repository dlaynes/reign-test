import React, { useEffect, useState } from 'react';
import { httpGetLatestStories } from '../services/http/api';
import { ListItem } from './ListItem';

export function List(props){
    const [news, setNews] = useState([]);

    useEffect(function(){
        (async function(){
            try {
                const stories = await httpGetLatestStories();
                if(stories){
                    setNews(stories);
                }
            } catch(e) {
                console.log(e);
                window.alert("Error when retrieving the latest stories");
            }
        }());
    }, []);

    return (
        <ul className="list">
            {news.map(item => <ListItem key={"news"+item.objectID} item={item} />)}
        </ul>
    );
}
