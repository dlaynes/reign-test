export function truncate(str, len_=20){
    if(str.length < len_) return str;

    return str.substr(0, len_);
}
