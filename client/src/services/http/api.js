import instance from "./base";

export async function httpGetLatestStories(){
    const response = await instance.get('topic', {});
    return response.data;
}

export async function httpRemoveStory(docId){
    const response = await instance.delete(`topic/${docId}`, {});
    return response.data;
}
