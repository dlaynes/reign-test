import axios from 'axios';

const baseUrl = process.env.REACT_APP_API_URL ? process.env.REACT_APP_API_URL : 'http://localhost:3001';

console.log("Base URL", baseUrl);
const instance = axios.create({
    baseURL : baseUrl,
    withCredentials: true,
    timeout: 1000000
});

export default instance;
