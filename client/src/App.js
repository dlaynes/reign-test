import React from 'react';

import './assets/normalize.css';
import './assets/App.css';
import { Header } from './components/Header';
import { List } from './components/List';

import TimeAgo from 'javascript-time-ago'

import en from 'javascript-time-ago/locale/en';

TimeAgo.addDefaultLocale(en);

function App() {
  return (
    <React.Fragment>
        <Header />
        <main className="app">
            <List />
        </main>
    </React.Fragment>
  );
}

export default App;
