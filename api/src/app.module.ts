import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';

import configuration, { DATABASE_HOST } from './config/configuration';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TopicModule } from './topic/topic.module';
import { HackerNewsService } from './hacker-news/hacker-news.service';

@Module({
    imports: [
        MongooseModule.forRoot(DATABASE_HOST),
        ScheduleModule.forRoot(),
        ConfigModule.forRoot({
            load: [configuration]
        }),
        HttpModule,
        TopicModule
    ],
    controllers: [AppController],
    providers: [AppService, HackerNewsService],
})
export class AppModule {}
