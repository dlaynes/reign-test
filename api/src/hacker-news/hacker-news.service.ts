import { HttpService, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cron } from '@nestjs/schedule';
import { Topic } from 'src/topic/schemas/topic.schema';
import { TopicService } from 'src/topic/topic.service';

@Injectable()
export class HackerNewsService {
    private readonly logger = new Logger(HackerNewsService.name);

    constructor(
        private topicService: TopicService,
        private httpService: HttpService,
        private configService: ConfigService
    ){

    }

    @Cron('* 0 * * * *')
    async handleCron() {
        const url = this.configService.get('apiUrl');

        try {
            const data = await this.httpService.get<{hits: Topic[]}>(url);
            data.subscribe((res) => {
                if(res.data.hits){
                    this.logger.debug("Obtained " + res.data.hits.length.toString()+" stories");

                    this.topicService.createMany(res.data.hits);
                }
            });
        } catch(e){
            //console.log()
            this.logger.warn("There was an error when loading the data: " + e.message);
        }
    }
}
