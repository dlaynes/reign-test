export const DATABASE_HOST = process.env.DATABASE_HOST || 'mongodb://localhost/reign-test';

export default () => ({
    database: DATABASE_HOST,
    apiUrl: 'http://hn.algolia.com/api/v1/search_by_date?query=nodejs'
});
