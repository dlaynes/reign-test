import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Topic, TopicDocument } from './schemas/topic.schema';
import { CreateTopicDto } from './dto/create-topic.dto';

@Injectable()
export class TopicService {
    constructor(@InjectModel(Topic.name) private topicModel: Model<TopicDocument>) {}

    async createMany(createDtos: CreateTopicDto[]) : Promise<boolean>{
        try {
            await createDtos.reduce(async(prev: any, item : any) => {
                await prev;
                //console.log("Creating", item);

                if(! await this.topicModel.exists({objectID: item.objectID})){
                    await this.topicModel.create(item);
                    //console.log("Topic created", item.objectID);
                } else {
                    //console.log("Topic already exists", item.objectID);
                }
                return Promise.resolve();
            }, Promise.resolve() );

            return true;
        } catch(e){
            return false;
        }
    }

    async markAsDeleted(objectID: string) : Promise<boolean>{
        return await this.topicModel.updateOne({objectID}, {deleted: true});
    }

    async findAll(): Promise<Topic[]> {
        return this.topicModel.find({deleted: false}, null, {sort: {created_at_i: -1}});
    }

}
