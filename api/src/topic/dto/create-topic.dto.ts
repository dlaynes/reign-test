export interface CreateTopicDto {
    created_at: string;
    title?: string;
    url?: string;
    author: string;
    points?: number;
    story_text?: string;
    story_id?: number;
    story_title: string;
    story_url: string;
//    parent_id: number;
//    created_at_i: number;
//    tags: string[];
    objectID: string;
}
