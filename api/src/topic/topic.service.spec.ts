import { Test, TestingModule } from '@nestjs/testing';
import { TopicService } from './topic.service';

describe('TopicService', () => {
  let service: TopicService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TopicService],
    }).compile();

    service = module.get<TopicService>(TopicService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of stories', async () => {

    });

    it('should order the stories by newest', async () => {

    });
  });

  describe('createMany', () => {

  });

  describe('markAsDeleted', () => {

  });

});
