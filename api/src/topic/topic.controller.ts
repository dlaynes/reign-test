import { Controller, Req, Param, Request, Get, Delete } from '@nestjs/common';
import { Topic } from './schemas/topic.schema';
import { TopicService } from './topic.service';

@Controller('topic')
export class TopicController {

    constructor(private topicService: TopicService){

    }

    @Get()
    async findAll(): Promise<Topic[]> {
        const data = await this.topicService.findAll();
        console.log("Results", data);

        return data;
    }

    @Delete(':id')
    async hideTopic(@Param() params): Promise<boolean> {
        return this.topicService.markAsDeleted(params.id);
    }

}
