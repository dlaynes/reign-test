import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type TopicDocument = Topic & Document;

@Schema()
export class Topic {

    @Prop()
    created_at: string;

    @Prop()
    title?: string;

    @Prop()
    url?: string;

    @Prop()
    author: string;

    @Prop()
    points?: number;

    @Prop()
    story_text?: string;

    @Prop()
    story_id?: number;

    @Prop()
    story_title: string;

    @Prop()
    story_url: string;

    /*
    @Prop()
    parent_id: number;

    @Prop()
    tags: string[];
    */

    @Prop({default: 0})
    created_at_i: number;

    @Prop({unique: true, index: true})
    objectID: string;

    @Prop({default: false})
    deleted?: boolean;

    /*
    @Prop([])
    _highlightResult: HighlightResult;
    */
}

export const TopicSchema = SchemaFactory.createForClass(Topic);
