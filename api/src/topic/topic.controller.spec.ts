import { Test, TestingModule } from '@nestjs/testing';
import { TopicController } from './topic.controller';
import { TopicService } from './topic.service';

describe('TopicController', () => {
  let controller: TopicController;
  let service: TopicService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TopicController],
      providers: [TopicService]
    }).compile();

    controller = module.get<TopicController>(TopicController);
    service = module.get<TopicService>(TopicService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of stories', async () => {
        const result = Promise.resolve([]); //An empty array for now
        jest.spyOn(service, 'findAll').mockImplementation(() => result);

        expect(await controller.findAll()).toBe(result);
    });
  });

  describe('hideTopic', () => {

  });

});
