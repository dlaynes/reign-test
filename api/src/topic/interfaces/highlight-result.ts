export interface HighlightResult {
    author: {
        value: string;
        matchLevel: string;
        matchedWords: string[];
    };
    comment_text: {
        value: string;
        matchLevel: string;
        fullyHighlighted: boolean;
        matchedWords: string[];
    };
    story_title: {
        value: string;
        matchLevel: string;
        matchedWords: string[];
    };
    story_url: {
        value: string;
        matchLevel: string;
        matchedWords: string[];
    };
}
