import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
    const app = await NestFactory.create(AppModule, {
        cors: {
            origin: true,
            credentials: true,
            methods: ['GET','DELETE']
        }
    });
    await app.listen(process.env.APP_PORT || 3001);
}
bootstrap();
